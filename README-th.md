[For English, click here](./README.md)

# Laraland

เป็น Web/REST server เขียนโดย PHP + Laravel

## คุณสมบัติ

ตัวอย่างเหล่านี้ สาธิต:

* ระบบสมาชิก
* ส่งเมล
* ลบ records ที่ไม่ได้ใช้แล้ว, สามารถจะกำหนดค่าได้
* อัพโหลด profile icon
* เปลี่ยนภาษา: อังกฤษ, ไทย
* เปลี่ยนธีมสี
* เข้าสู่ระบบโดย Google และ LINE, บน web เท่านั้น

## Screenshots

รูปภาพหน้าจอ อยู่ใน "%SOURCE%/samples.laraland/screenshots"

## การติดตั้ง

ถ้าคุณยังไม่ได้ติดตั้ง โปรแกรมสำหรับพัฒนา ติดตั้งได้แล้ว:

* [PHP](https://php.net), สำหรับการเขียนโปรแกรม PHP
* [Laravel](https://laravel.com), สำหรับ framework web server
* [Vue](https://vuejs.org) และ [web browser](https://www.google.com/chrome), สำหรับ web client
* [Postman](https://www.postman.com), สำหรับทดสอบ server

## Run Server

Source code สำหรับ web server อยู่ใน "samples.laraland/laraland"

เปลี่ยน folder:

	cd %SOURCE%/samples.laraland/laraland

ติดตั้ง, ทำครั้งเดียวเท่านั้น:

	composer install
	php artisan storage:link

จากนั้น สร้าง MySQL database เปล่า ชื่อ laraland, จากนั้นสุดท้าย run script:

	php artisan migrate:refresh

Run:

	php artisan serve

สำหรับการ run ครั้งแรก, ใช้โปรแกรม REST client เช่น: Postman, โปรด run:

	POST http://localhost:8000/api/setup

โปรแกรมจะสร้างสมาชิก "admin@your.diy" กับรหัสผ่าน "admin"

้ถ้าคุณต้องการ run โปรแกรม cleanup, เพื่อลบ logs หรือข้อมูลสมัครที่ หรือข้อมูลเปลี่ยน password ที่เก่าแล้ว, run:

	php artisan command:clear

หรือ run เป็นระยะ อัตโนมัติ:

	php artisan schedule:work

## Run ทดสอบ Server

ถ้าคุณต้องการทดสอบ server เปิดโปรแกรม Postman และ import scripts "laraland.postman_collection" และ "laraland.postman_environment" ใน folder "%SOURCE%/samples.laraland/laraland"  อย่าลืม set Environments เป็น "laraland"

## Run Web

โดยปกติ Laraland จะบริการ REST API เท่านั้น ถ้าคุณต้องการ run web:

ติดตั้ง, ทำครั้งเดียวเท่านั้น:

	npm i

Run:

	npm run dev

สุดท้าย, เปิด browser และใส่ URL:

	http://localhost:8000

## Run API Client

เนื่องจาก REST server มีความ compatible กับ [Coldfire project](https://gitlab.com/taworn.ta7/samples.coldfire) คุณสามารถใช้ทั้ง Flutter app และ web clients ใน project Coldfire ได้ทันที  Download [Coldfire](https://gitlab.com/taworn.ta7/samples.coldfire) และเปิด README เพิ่มเติม  เมื่อต้องการ run laraland, พิมพ์:

	php artisan serve --port=6600

เพื่อเปลี่ยนเลข port ให้เหมือนกับ Coldfire

## การกำหนดค่า

มีไฟล์ชื่อ ".env" อยู่ในต้นทาง folder  มันเก็บการกำหนดค่าและสามารถเปลี่ยนได้

รายการกำหนดค่า:

* DAYS_TO_KEEP_LOGS: จำนวนวันที่จะเก็บค่าเก่า ก่อนลบ log
* DAYS_TO_KEEP_DBLOGS: จำนวนวันที่จะเก็บค่าเก่า database log
* DAYS_TO_KEEP_SIGNUPS: จำนวนวันที่จะเก็บค่าเก่า สำหรับ records ในตาราง member_signup
* DAYS_TO_KEEP_RESETS: จำนวนวันที่จะเก็บค่าเก่า สำหรับ records ในตาราง member_reset
* DB_USERNAME: user ที่ใช้
* DB_PASSWORD: รหัสผ่านสำหรับเข้า database
* MAIL_HOST: เครื่องที่ต่อ สำหรับส่งเมล
* MAIL_PORT: port ที่ใช้
* MAIL_USERNAME: user ที่ใช้
* MAIL_PASSWORD: รหัสผ่านสำหรับเข้าเมล

และยังมีค่าที่เก็บไว้ใน "config/env.php":

* authen_timeout: เวลาก่อนที่ session จะหมดอายุ หน่วยเป็น มิลลิวินาที
* profile_icon_file_limit: จำนวน bytes ที่จะจำกัด ตอนอัพโหลด profile icon

อย่าลืม สร้าง database เปล่า ชื่อ "laraland"

ผมใช้ [ethereal](https://ethereal.email) ซึ่งเป็น mail service ปลอม สำหรับทดสอบ
และบางครั้ง ชื่อผู้ใช้กับรหัสผ่านจะหมดอายุ และคุณต้องสร้างขึ้นมาใหม่ ไม่เสียค่าใช้จ่าย

## เอกสารสำหรับการเขียนโปรแกรม

เอกสารเพิ่มเติม:

* [APIs ทั้งหมด](./docs/APIs.md)
* [การจัดการ Error สำหรับ Client](./docs/Error%20Handling%20for%20Client.md)
* [การจัดเรียงลำดับ](./docs/Sort%20Ordering.md)

## สุดท้าย

ขอโทษครับ แต่ผมไม่เก่งภาษาอังกฤษ T_T
