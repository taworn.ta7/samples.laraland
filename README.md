[สำหรับภาษาไทย คลิกที่นี่](./README-th.md)

# Laraland

This is a Web/REST server project.  Written by PHP + Laravel.

## Features

These samples demostration of features:

* Members system
* Send mail
* Delete unused records, configurable
* Upload profile icon
* Change language: English, Thai
* Change theme color
* Sign-in to Google and LINE, on web version only

## Screenshots

Screenshot pictures are in "%SOURCE%/samples.laraland/screenshots".

## Installation

If you not installed development tools, installs them now:

* [PHP](https://php.net), for programming language
* [Laravel](https://laravel.com), for framework web server
* [Vue](https://vuejs.org) and [modern web browser](https://www.google.com/chrome), for web client
* [Postman](https://www.postman.com), for testing server

## Run Server

Source code for web server are in "samples.laraland/laraland"

Changes folder to:

	cd %SOURCE%/samples.laraland/laraland

Setup, one-time only:

	composer install
	php artisan storage:link

then, create blank MySQL database: laraland, and finally, run migrate script:

	php artisan migrate:refresh

Run:

	php artisan serve

For the first run, uses REST client software, such as: Postman, to run:

	POST http://localhost:8000/api/setup

It will create member "admin@your.diy" with password "admin".

If you want to run cleanup, delete unused logs, sign-up and reset, run:

	php artisan command:clear

Or, run periodically:

	php artisan schedule:work

## Run Server Test

If you want to run test server, open Postman and import scripts "laraland.postman_collection" and "laraland.postman_environment" in folder "%SOURCE%/samples.laraland/laraland".  Don't forget to set Environments to "laraland".

## Run Web

By default, Laraland will serve REST API only.  If you want to run web:

Setup, one-time only:

	npm i

Run:

	npm run dev

Finally, opens browser to URL:

	http://localhost:8000

## Run API Client

Because the REST server is compatible with [Coldfire project](https://gitlab.com/taworn.ta7/samples.coldfire).  You can use both Flutter app and web clients in project Coldfire.  Download [Coldfire](https://gitlab.com/taworn.ta7/samples.coldfire) and see README.  When run laraland, type:

	php artisan serve --port=6600

to change port number to Coldfire.

## Configuration

There is a file called ".env" in the root of server folder.  It keeps configuration and can be changes.

Here are some list:

* DAYS_TO_KEEP_LOGS: number of days to keep old log file
* DAYS_TO_KEEP_DBLOGS: number of days to keep old database log
* DAYS_TO_KEEP_SIGNUPS: number of days to keep old records in member_signup table
* DAYS_TO_KEEP_RESETS: number of days to keep old records in member_reset table
* DB_USERNAME: database user
* DB_PASSWORD: database password
* MAIL_HOST: mail sender host
* MAIL_PORT: mail sender port
* MAIL_USERNAME: mail sender user
* MAIL_PASSWORD: mail sender password

and additional settings in "config/env.php":

* authen_timeout: time before session expire, as milli-seconds
* profile_icon_file_limit: number of bytes to limit upload profile icon

Don't forget to create blank database, named "laraland".

I used service [ethereal](https://ethereal.email), which is a fake mail service.
And, some time, your user and password will expire and you have to recreate them.  It's free.

## Programming Documents

There are more documents:

* [All APIs](./docs/APIs.md)
* [Error Handling for Client](./docs/Error%20Handling%20for%20Client.md)
* [Sort Ordering](./docs/Sort%20Ordering.md)

## Last

Sorry, but I'm not good at English. T_T
