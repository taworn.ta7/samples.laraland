<?php

//use Illuminate\Support\Facades\Facade;

return [

	// app version
	'version' => '1.0.0',

	// days to keep...
	'days_to_keep_logs' => env('DAYS_TO_KEEP_LOGS', 1),
	'days_to_keep_dblogs' => env('DAYS_TO_KEEP_DBLOGS', 1),
	'days_to_keep_signups' => env('DAYS_TO_KEEP_SIGNUPS', 1),
	'days_to_keep_resets' => env('DAYS_TO_KEEP_RESETS', 1),

	// milli-seconds until timeout
	'authen_timeout' => 600000,

	// profile icon size limit
	'profile_icon_file_limit' => 204800,

	// available generic settings
	'available_settings' => [
		'app-theme',
		'web-theme',
		'web-theme-element',
	],

	/**
	 * Google Client
	 */
	'google_client_id' => '1028843747070-gs1edoi3su1a29qd6s6vt5kdafdqejqd.apps.googleusercontent.com',
	'google_client_secret' => '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',
	'google_redirect_url' => 'http://localhost:8000/authenx/google',

	/**
	 * LINE Client
	 */

	'line_client_id' => '1657191325',
	'line_client_secret' => 'b3ee7daae9b6b4e6cb64213d109ca2fc',
	'line_redirect_url' => 'http://localhost:8000/authenx/line',

];
