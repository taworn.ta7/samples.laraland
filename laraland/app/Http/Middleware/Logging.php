<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Logging
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
	 * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
	 */
	public function handle(Request $request, Closure $next)
	{
		$request->attributes->add(['request_id' => Str::orderedUuid()]);
		$request_id = $request->attributes->get('request_id');

		Log::info("$request_id; {$request->method()} {$request->path()}...");
		$response = $next($request);
		if (method_exists($response, 'status')) {
			$status = $response->status();
			if ($status >= 200 && $status < 300) {
				Log::info("$request_id; {$request->method()} {$request->path()}: DONE");
			}
		}

		return $response;
	}
}
