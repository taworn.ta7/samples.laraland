<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'authen.php'));

class AuthenAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $member = memberFromHeaders($request);
        if ($member->role !== \App\Models\MemberRole::Admin->value)
            throw \App\Exceptions\UnauthorizedException::withAdminRequired();
        $request->attributes->add(['member' => $member]);
        $request_id = $request->attributes->get('request_id');
        Log::info("$request_id; $member->id/$member->email [$member->role] name=$member->name");
        return $next($request);
    }
}
