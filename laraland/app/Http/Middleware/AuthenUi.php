<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'authen.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'settings', 'generic.php'));

class AuthenUi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $request_id = $request->attributes->get('request_id');

        try {
            $token = $request->session()->get('token');
            $member = memberFromToken($token);
        } catch (\Exception) {
            $member = null;
        }

        $request->attributes->add(['member' => $member]);
        if ($member) {
            $settings = \App\Classes\settingsGetAll($member);
            $request->attributes->add(['settings' => $settings]);
            Log::info("$request_id; $member->id/$member->email [$member->role] name=$member->name");
        }
        return $next($request);
    }
}
