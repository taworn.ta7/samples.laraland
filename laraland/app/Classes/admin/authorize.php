<?php

namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Administration Services / Members
// ----------------------------------------------------------------------

/**
 * Authorizes the member signing-up.  This service is primary used in testing only.
 */
function adminMembersAuthorize(Request $request)
{
	$request_id = $request->attributes->get('request_id');

	// validates request
	$code = $request->query('code', '');
	if (trim($code) === '')
		throw \App\Exceptions\NotFoundException::withNotExists();
	$signup = \App\Models\MemberSignUp::where('confirm_token', $code)->first();
	if (!$signup)
		throw \App\Exceptions\NotFoundException::withNotExists();

	// checks if sign-up data already created member or not
	$member = \App\Models\Member::where('email', $signup->email)->first();
	if ($member)
		throw \App\Exceptions\ForbiddenException::withAlreadyExists();

	// generates name from email
	$names = preg_split("/\@/", $signup->email);
	$name = $names[0];

	// creates member
	DB::beginTransaction();
	try {
		$member = \App\Models\Member::create([
			'email' => $signup->email,
			'role' => \App\Models\MemberRole::Member->value,
			'locale' => $signup->locale,
			'name' => $name,
			'disabled' => null,
			'resigned' => null,
			'begin' => null,
			'end' => null,
			'expire' => null,
		]);
		$credential = \App\Models\MemberCredential::create([
			'id' => $member->id,
			'salt' => $signup->salt,
			'hash' => $signup->hash,
			'token' => null,
		]);
		DB::commit();
	} catch (\Exception $ex) {
		DB::rollBack();
		throw \App\Exceptions\InternalServerException::withDbTransaction($ex->getMessage());
	}

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email is sign-up and confirm.",
	]);

	Log::debug("$request_id; member created: {$member->toJson(JSON_PRETTY_PRINT)}");
	return [
		'member' => $member,
	];
}
