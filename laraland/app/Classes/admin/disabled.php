<?php

namespace App\Classes;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Administration Services / Members
// ----------------------------------------------------------------------

/**
 * Disables or enables a member.
 */
function adminMembersDisable(Request $request, string $email, bool $disabled)
{
	$request_id = $request->attributes->get('request_id');
	$admin = $request->attributes->get('member');

	// selects member
	$member = \App\Models\Member::where('email', $email)->first();
	if (!$member)
		throw \App\Exceptions\NotFoundException::withNotExists();

	// updates member
	$member->disabled = $disabled ? \Carbon\Carbon::now() : null;
	$member->end = null;
	$member->credential->token = null;
	DB::Transaction(function () use ($member) {
		$member->save();
		$member->credential->save();
	});

	// adds log
	\App\Models\Logging::create([
		'member_id' => $admin->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Admin $admin->id/$admin->email changed $member->id/$member->email, disabled = $disabled.",
	]);

	return [
		'member' => reduceMemberData($member),
	];
}
