<?php

namespace App\Classes;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Settings Services / Generic Settings
// ----------------------------------------------------------------------

/**
 * Sets key-value generic settings.
 */
function settingsPut(\App\Models\Member $member, string $key, string $value)
{
	// checks with available settings list
	$list = config('env.available_settings');
	if (!in_array($key, $list))
		throw \App\Exceptions\NotFoundException::withNotExists();

	// upserts
	$value = substr($value, 0, 512);
	$setting = \App\Models\MemberSettings::where('member_id', $member->id)->where('key', $key)->first();
	if ($setting) {
		$setting->value = $value;
		$setting->save();
	} else {
		\App\Models\MemberSettings::create([
			'member_id' => $member->id,
			'key' => $key,
			'value' => $value,
		]);
	}

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'upsert',
		'table' => 'member_settings',
		'description' => "Member $member->id/$member->email is set $key=$value.",
	]);

	return [
		'key' => $key,
		'value' => $setting ? $setting->value : null,
	];
}

/**
 * Gets key-value generic settings.  Returns value string, or null if key not exists.
 */
function settingsGet(\App\Models\Member $member, string $key)
{
	$setting = \App\Models\MemberSettings::where('member_id', $member->id)->where('key', $key)->first();
	return [
		'value' => $setting ? $setting->value : null,
	];
}

/**
 * Gets all generic settings.  Returns all key-value pairs.
 */
function settingsGetAll(\App\Models\Member $member)
{
	$settings = \App\Models\MemberSettings::where('member_id', $member->id)->get();
	$result = [];
	foreach ($settings as $item) {
		$result[$item->key] = $item->value;
	};
	// dummy to get result {} or []
	$result['__DUMMY__'] = '';

	return [
		'settings' => $result,
	];
}

/**
 * Deletes all generic settings.  This function is use for reset.
 */
function settingsReset(\App\Models\Member $member)
{
	\App\Models\MemberSettings::where('member_id', $member->id)->delete();

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'delete',
		'table' => 'member_settings',
		'description' => "Member $member->id/$member->email is reset settings.",
	]);

	return [];
}
