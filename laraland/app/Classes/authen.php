<?php

namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'authen.php'));



// ----------------------------------------------------------------------
// Authentication Services
// ----------------------------------------------------------------------

/**
 * Authorizes the email and password.  Returns member data and sign-in token and will be use all the session.
 */
function authenSignIn(Request $request, string $email, string $password)
{
	$request_id = $request->attributes->get('request_id');

	// selects member
	$member = \App\Models\Member::where('email', $email)->first();
	if (!$member)
		throw \App\Exceptions\UnauthorizedException::withEmailOrPasswordInvalid();
	if (!Hash::check($password, $member->credential->hash))
		throw \App\Exceptions\UnauthorizedException::withEmailOrPasswordInvalid();

	// checks if this member resigned or disabled
	if ($member->resigned)
		throw \App\Exceptions\UnauthorizedException::withMemberIsResigned();
	if ($member->disabled)
		throw \App\Exceptions\UnauthorizedException::withMemberIsDisabledByAdmin();

	// checks if expiry sign-in
	$token = newOrUpdateToken($member);

	// updates member
	DB::Transaction(function () use ($member) {
		$member->save();
		$member->credential->save();
	});

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email is sign-in.",
	]);

	Log::info("$request_id; member sign-in: $member->id/$member->email [$member->role] $member->name");
	return [
		'member' => reduceMemberData($member),
		'token' => $token,
	];
}

/**
 * Signs off from current session.  The sign-in token will be invalid.
 */
function authenSignOut(Request $request)
{
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');
	$request->attributes->remove('member');
	if (!$member)
		return [];

	// updates member
	$member->end = \Carbon\Carbon::now();
	$member->credential->token = null;
	DB::Transaction(function () use ($member) {
		$member->save();
		$member->credential->save();
	});

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email is sign-out.",
	]);

	Log::info("$request_id; member sign-out: $member->id/$member->email [$member->role] $member->name");
	return [
		'member' => reduceMemberData($member),
	];
}
