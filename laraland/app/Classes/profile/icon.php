<?php

namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Icon
// ----------------------------------------------------------------------

/**
 * Uploads picture to use as member profile icon.
 */
function profileIconUpload(Request $request)
{
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');

	// checks uploaded image
	if (!$request->hasFile('image'))
		throw \App\Exceptions\BadRequestException::withValidationFailed();
	$file = $request->file('image');
	dumpvar($file, "image");
	if (!$file->isValid())
		throw \App\Exceptions\BadRequestException::withUploadIsNotFound();
	$mime = $file->getClientMimeType();
	if ($mime !== 'image/png' && $mime !== 'image/jpeg' && $mime !== 'image/gif')
		throw \App\Exceptions\BadRequestException::withUploadIsNotTypeImage();
	$limit = config('env.profile_icon_file_limit') / 1024;
	try {
		$input = $request->validate([
			'image' => 'max:' . $limit,
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withUploadIsTooBig($ex->getMessage(), $limit = "$limit KB");
	}

	// put uploaded file
	$dir = "public/$member->id/profile";
	$part = "storage/$member->id/profile";
	$name = 'icon';
	Storage::disk('local')->putFileAs($dir, $file, $name);

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email uploaded profile icon.",
	]);

	return [
		'image' => [
			'protocol' => $request->getScheme(),
			'host' => $request->getSchemeAndHttpHost(),
			'path' => $part,
			'name' => $name,
			'url' => "{$request->getSchemeAndHttpHost()}/$part/$name",
		],
	];
}

/**
 * Views the uploaded profile icon.
 */
function profileIconView(Request $request)
{
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');

	Log::debug('1');
	$dir = "public/$member->id/profile";
	$part = "storage/$member->id/profile";
	$name = 'icon';
	$fs = Storage::disk('local');

	Log::debug('2');
	if ($fs->fileExists("$dir/$name")) {
		Log::debug('3');
		return redirect("$part/$name");
	} else
		return redirect("assets/default-profile-icon.png");
}

/**
 * Deletes the uploaded profile icon and reverts profile icon to defaults.
 */
function profileIconDelete(Request $request)
{
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');

	$dir = "public/$member->id/profile";
	$name = 'icon';
	Storage::disk('local')->delete("$dir/$name");

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email deleted profile icon.",
	]);

	return [];
}
