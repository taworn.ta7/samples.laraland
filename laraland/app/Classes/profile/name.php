<?php

namespace App\Classes;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Name
// ----------------------------------------------------------------------

/**
 * Changes the current name.
 */
function profileChangeName(Request $request, \App\Models\Member $member, string $name)
{
	// set name
	$member->name = $name;
	$member->save();

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email changed name.",
	]);

	return [
		'member' => reduceMemberData($member),
	];
}
