<?php

namespace App\Classes;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Password
// ----------------------------------------------------------------------

/**
 * Changes the current password.  After the password is changed, you will be sign-out and need to sign-in again.
 */
function profileChangePass(
	Request $request,
	\App\Models\Member $member,
	string $currentPassword,
	string $newPassword,
) {
	$request_id = $request->attributes->get('request_id');

	// set password
	if (!Hash::check($currentPassword, $member->credential->hash))
		throw \App\Exceptions\UnauthorizedException::withPasswordIsIncorrect();
	$password = Hash::make($newPassword);
	$member->end = \Carbon\Carbon::now();
	$member->credential->salt = '';
	$member->credential->hash = $password;
	$member->credential->token = null;
	DB::Transaction(function () use ($member) {
		$member->save();
		$member->credential->save();
	});

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email changed password!",
	]);

	return [
		'member' => reduceMemberData($member),
	];
}
