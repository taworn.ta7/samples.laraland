<?php

namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Members Services / Sign Up
// ----------------------------------------------------------------------

/**
 * Receives password reset form and sends email to member.
 */
function membersRequestReset(Request $request, string $email)
{
	$request_id = $request->attributes->get('request_id');

	// first, checks this input email
	// this email must be valid in member table
	$member = \App\Models\Member::where('email', $email)->first();
	if (!$member)
		throw \App\Exceptions\NotFoundException::withNotExists();

	// creates reset
	$reset = \App\Models\MemberReset::create([
		'email' => $email,
		'confirm_token' => Str::random(32),
	]);

	// creates url
	$url = "{$request->getSchemeAndHttpHost()}/api/members/reset-password?code=$reset->confirm_token";
	Log::debug("$request_id; url: $url");

	// sends email
	$params = [
		'email' => $reset->email,
		'token' => $reset->confirm_token,
		'url'   => $url,
	];
	Mail::to($reset->email)->send(new \App\Mail\MemberReset($params));

	return [
		'member' => $reset,
		'url' => $url,
	];
}

/**
 * Confirms the password reset.  This will generate new password and send new password to email.
 */
function membersReset(Request $request)
{
	$request_id = $request->attributes->get('request_id');

	// validates request
	$code = $request->query('code', '');
	if (trim($code) === '')
		throw \App\Exceptions\NotFoundException::withNotExists();
	$reset = \App\Models\MemberReset::where('confirm_token', $code)->first();
	if (!$reset)
		throw \App\Exceptions\NotFoundException::withNotExists();

	// checks if reset data must be valid
	$member = \App\Models\Member::where('email', $reset->email)->first();
	if (!$member)
		throw \App\Exceptions\NotFoundException::withNotExists();
	$member->credential;

	// generates new password
	$generate = Str::random(8);
	$password = Hash::make($generate);
	$member->credential->salt = '';
	$member->credential->hash = $password;
	$member->credential->token = null;
	Log::debug("$request_id; member password reset: {$member->toJson(JSON_PRETTY_PRINT)}");

	// saves
	$member->credential->save();

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email is reset password.",
	]);

	// sends mail
	$params = [
		'email'    => $member->email,
		'password' => $generate,
	];
	Mail::to($member->email)->send(new \App\Mail\NewPassword($params));

	return [
		'member' => reduceMemberData($member),
	];
}
