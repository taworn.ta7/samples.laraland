<?php

namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Members Services / Sign Up
// ----------------------------------------------------------------------

/**
 * Receives sign-up form and saves it to database.  Returns URL to let user confirm it.
 */
function membersSignUp(Request $request, string $email, string $password, string $locale)
{
	$request_id = $request->attributes->get('request_id');

	// first, checks this input email
	// this email must not be in member table
	$signup = \App\Models\Member::where('email', $email)->first();
	if ($signup)
		throw \App\Exceptions\ForbiddenException::withAlreadyExists();

	// creates sign-up
	$password = Hash::make($password);
	$signup = \App\Models\MemberSignUp::create([
		'email' => $email,
		'locale' => $locale,
		'salt' => '',
		'hash' => $password,
		'confirm_token' => Str::random(32),
	]);

	// creates url
	$url = "{$request->getSchemeAndHttpHost()}/api/members/signup/confirm?code=$signup->confirm_token";
	Log::debug("$request_id; url: $url");

	// sends email
	$params = [
		'email' => $signup->email,
		'token' => $signup->confirm_token,
		'url'   => $url,
	];
	Mail::to($signup->email)->send(new \App\Mail\MemberSignUp($params));

	return [
		'member' => $signup,
		'url' => $url,
	];
}

/**
 * Confirms the sign-up form, then, copies sign-up data into member table.
 */
function membersSignUpConfirm(Request $request)
{
	$request_id = $request->attributes->get('request_id');

	// validates request
	$code = $request->query('code', '');
	if (trim($code) === '')
		throw \App\Exceptions\NotFoundException::withNotExists();
	$signup = \App\Models\MemberSignUp::where('confirm_token', $code)->first();
	if (!$signup)
		throw \App\Exceptions\NotFoundException::withNotExists();

	// checks if sign-up data already created member or not
	$member = \App\Models\Member::where('email', $signup->email)->first();
	if ($member)
		throw \App\Exceptions\ForbiddenException::withAlreadyExists();

	// generates name from email
	$names = preg_split("/\@/", $signup->email);
	$name = $names[0];

	// creates member
	DB::beginTransaction();
	try {
		$member = \App\Models\Member::create([
			'email' => $signup->email,
			'role' => \App\Models\MemberRole::Member->value,
			'locale' => $signup->locale,
			'name' => $name,
			'disabled' => null,
			'resigned' => null,
			'begin' => null,
			'end' => null,
			'expire' => null,
		]);
		$credential = \App\Models\MemberCredential::create([
			'id' => $member->id,
			'salt' => $signup->salt,
			'hash' => $signup->hash,
			'token' => null,
		]);
		DB::commit();
	} catch (\Exception $ex) {
		DB::rollBack();
		throw \App\Exceptions\InternalServerException::withDbTransaction($ex->getMessage());
	}

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email is sign-up and confirm.",
	]);

	Log::debug("$request_id; member created: {$member->toJson(JSON_PRETTY_PRINT)}");
	return [
		'member' => $member,
	];
}
