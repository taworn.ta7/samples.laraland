<?php

namespace App\Classes;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'pagination.php'));



// ----------------------------------------------------------------------
// Members Services
// ----------------------------------------------------------------------

/**
 * Lists members with conditions.  All parameters are optional.
 */
function membersList(Request $request)
{
	$request_id = $request->attributes->get('request_id');

	// gets paging query
	$pagi = queryPagingData($request, [
		'email' => 'email',
		'name' => 'name',
		'created' => 'created',
		'updated' => 'updated',
	]);
	$pageSize = $request->query('size', '');
	//$pageSize = $request->query('size', '1');
	$rowsPerPage = (int)($pageSize && $pageSize > 0 ? $pageSize : 10);

	// finds and counts
	$search = $pagi['search'];
	$trash = $pagi['trash'];
	$query = \App\Models\Member::where('role', '!=', \App\Models\MemberRole::Admin->value);
	if ($search) {
		$find = "%$search%";
		$query->where(function ($query) use ($find) {
			$query->where('email', 'like', $find)->orWhere('name', 'like', $find);
		});
	}
	if (!$trash) {
		$query->where(function ($query) {
			$query->whereNull('disabled')->whereNull('resigned');
		});
	} else {
		$query->where(function ($query) {
			$query->whereNotNull('disabled')->orWhereNotNull('resigned');
		});
	}
	foreach ($pagi['order'] as $key => $value) {
		$query->orderBy($key, $value);
	}
	$count = $query->count();
	$query->skip($pagi['page'] * $rowsPerPage)->take($rowsPerPage);
	dumpvar($query->toSql(), "query");
	$items = $query->get();

	return [
		'members' => $items,
		'pagination' => getPagination($pagi['page'], $rowsPerPage, $count),
	];
}

/**
 * Gets member's information.
 */
function membersGet(Request $request, string $email)
{
	$item = \App\Models\Member::where('email', $email)->first();
	if (!$item)
		throw \App\Exceptions\NotFoundException::withNotExists();
	return [
		'member' => reduceMemberData($item),
	];
}

/**
 * Gets member's icons.
 */
function membersGetIcon(Request $request, string $email)
{
	$item = \App\Models\Member::where('email', $email)->first();
	if (!$item)
		throw \App\Exceptions\NotFoundException::withNotExists();

	$dir = "public/$item->id/profile";
	$part = "storage/$item->id/profile";
	$name = 'icon';
	$fs = Storage::disk('local');

	if ($fs->fileExists("$dir/$name"))
		return redirect("$part/$name");
	else
		return redirect("assets/default-profile-icon.png");
}
