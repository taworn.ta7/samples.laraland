<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberCredential extends Model
{
    use HasFactory;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $attributes = [
        'id' => null,
        'salt' => '',
        'hash' => '',
        'token' => null,
    ];

    protected $fillable = [
        'id',
        'salt',
        'hash',
        'token',
    ];

    public $incrementing = false;

    protected $keyType = 'string';

    public function member()
    {
        return $this->belongsTo(Member::class, 'id');
    }
}
