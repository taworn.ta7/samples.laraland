<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logging extends Model
{
    use HasFactory;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'member_id' => '',
        'action' => '',
        'table' => '',
        'description' => '',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'member_id',
        'action',
        'table',
        'description',
    ];
}
