<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Model;

class MemberSettings extends Model
{
    use HasFactory;
    use HasUlids;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'member_id' => '',
        'key' => '',
        'value' => '',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'key',
        'value',
    ];

    public $incrementing = false;

    protected $keyType = 'string';
}
