<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Model;

enum MemberRole: string
{
    case Member = 'member';
    case Admin = 'admin';
}

class Member extends Model
{
    use HasFactory;
    use HasUlids;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'email' => '',
        'role' => 'member',
        'locale' => '',
        'name' => '',
        'disabled' => null,
        'resigned' => null,
        'begin' => null,
        'end' => null,
        'expire' => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'role',
        'locale',
        'name',
        'disabled',
        'resigned',
        'begin',
        'end',
        'expire',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'disabled' => 'datetime',
        'resigned' => 'datetime',
        'begin' => 'datetime',
        'end' => 'datetime',
        'expire' => 'datetime',
    ];

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The data type of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Get the credential associated with the member.
     */
    public function credential()
    {
        return $this->hasOne(MemberCredential::class, 'id');
    }
}
