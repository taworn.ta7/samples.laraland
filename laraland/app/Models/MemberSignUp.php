<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberSignUp extends Model
{
    use HasFactory;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'email' => '',
        'locale' => '',
        'salt' => '',
        'hash' => '',
        'confirm_token' => null,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'locale',
        'salt',
        'hash',
        'confirm_token',
    ];
}
