<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));

class ClearUnusedData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete unused data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->deleteLogs(config('env.days_to_keep_logs'));
        $this->deleteDbLogs(config('env.days_to_keep_dblogs'));
        $this->deleteSignUps(config('env.days_to_keep_signups'));
        $this->deleteResets(config('env.days_to_keep_resets'));
        return Command::SUCCESS;
    }

    // ----------------------------------------------------------------------

    private function deleteLogs(int $daysToKeep)
    {
        // checks before execute, days to keep must keep at least 0 (today) 
        if ($daysToKeep < 0)
            return;

        // computes date range
        $begin = \Carbon\Carbon::now()->addDay(-$daysToKeep);

        // loop for delete files
        Log::debug("logs older than {$begin->format('Y-m-d')} will be delete!");
        $storage = Storage::disk('logs');
        $files = $storage->files();
        for ($i = 0; $i < count($files); $i++) {
            $file = $files[$i];
            if (preg_match('/^laravel-([0-9]{4})-([0-9]{2})-([0-9]{2})\.log$/', $file, $matches, PREG_OFFSET_CAPTURE)) {
                $y = $matches[1][0];
                $m = $matches[2][0];
                $d = $matches[3][0];
                $date = \Carbon\Carbon::createFromDate($y, $m, $d);
                if ($date->timestamp < $begin->timestamp) {
                    Log::debug("delete: $file");
                    $storage->delete($file);
                }
            }
        }
    }

    // ----------------------------------------------------------------------

    private function deleteDbLogs(int $daysToKeep)
    {
        // checks before execute, days to keep must keep at least 0 (today) 
        if ($daysToKeep < 0)
            return;

        // computes date range
        $begin = \Carbon\Carbon::now()->addDay(-$daysToKeep);

        // cleanup old logging table
        Log::debug("database logs older than $begin will be delete!");
        \App\Models\Logging::where('created', '<', $begin)->delete();
    }

    // ----------------------------------------------------------------------

    private function deleteSignUps(int $daysToKeep)
    {
        // checks before execute, days to keep must keep at least 0 (today) 
        if ($daysToKeep < 0)
            return;

        // computes date range
        $begin = \Carbon\Carbon::now()->addDay(-$daysToKeep);

        // cleanup old member_signup table
        Log::debug("sign-ups data older than $begin will be delete!");
        \App\Models\MemberSignUp::where('created', '<', $begin)->delete();
    }

    // ----------------------------------------------------------------------

    private function deleteResets(int $daysToKeep)
    {
        // checks before execute, days to keep must keep at least 0 (today) 
        if ($daysToKeep < 0)
            return;

        // computes date range
        $begin = \Carbon\Carbon::now()->addDay(-$daysToKeep);

        // cleanup old member_reset table
        Log::debug("resets data older than $begin will be delete!");
        \App\Models\MemberReset::where('created', '<', $begin)->delete();
    }
}
