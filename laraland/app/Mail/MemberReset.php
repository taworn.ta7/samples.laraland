<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MemberReset extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(array $params)
	{
		$this->params = $params;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from(config('mail.from.address'), config('mail.from.name'))
			->subject('Please Reset Password Confirmation')
			->view('mail_templates/member_reset', [
				'email' => $this->params['email'],
				'token' => $this->params['token'],
				'url'   => $this->params['url'],
			]);
	}
}
