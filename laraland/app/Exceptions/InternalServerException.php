<?php

namespace App\Exceptions;

class InternalServerException extends CustomHttpException
{
	// 500 Internal Server Error
	public function __construct(string $message, array $locales)
	{
		parent::__construct(500, $message, $locales);
	}

	public static function withDbTransaction(string $message = '')
	{
		return new self($message, [
			'en' => "Database transaction cannot commit!",
			'th' => "ฐานข้อมูลไม่สามารถบันทึกได้!",
		]);
	}
}
