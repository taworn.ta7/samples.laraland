<?php

namespace App\Exceptions;

class NotFoundException extends CustomHttpException
{
	// 404 Not Found
	public function __construct(string $message, array $locales)
	{
		parent::__construct(404, $message, $locales);
	}

	public static function withNotExists(string $message = '')
	{
		return new self($message, [
			'en' => "Data is not exists!",
			'th' => "ข้อมูลที่ต้องการไม่มี!",
		]);
	}
}
