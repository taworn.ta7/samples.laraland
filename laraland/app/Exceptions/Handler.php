<?php

namespace App\Exceptions;

use Throwable;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (HttpException $e, $request) {
            $request_id = $request->attributes->get('request_id');
            $code = $e->getStatusCode();
            $text = array_key_exists($code, Response::$statusTexts) ? " " . Response::$statusTexts[$code] : "";

            $json = [
                'statusCode' => $e->getStatusCode(),
                'message'    => $e->getMessage(),
                'locales'    => null,
                'path'       => $request->path(),
                'requestId'  => $request_id,
                'timestamp'  => date('c'),
            ];
            if ($e instanceof CustomHttpException) {
                $json['locales'] = $e->locales;
            }
            $output = json_encode($json, JSON_PRETTY_PRINT);
            Log::error("$request_id; $code$text $output");

            if ($request->is('api/*')) {
                return response()->json($json, $e->getStatusCode());
            } else {
                $lang = $json['locales'];
                return response()->view('exception', [
                    'statusCode' => $json['statusCode'],
                    'message'    => $json['message'],
                    'locales'    => $lang ? "{$lang['en']} / {$lang['th']}" : "",
                    'path'       => $json['path'],
                    'requestId'  => $json['requestId'],
                    'timestamp'  => $json['timestamp'],
                ], $json['statusCode']);
            }
        });
    }
}
