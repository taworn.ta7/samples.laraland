<?php

namespace App\Exceptions;

class UnauthorizedException extends CustomHttpException
{
	// 401 Unauthorized
	public function __construct(string $message, array $locales)
	{
		parent::__construct(401, $message, $locales);
	}

	public static function withTimeout(string $message = '')
	{
		return new self($message, [
			'en' => "Timeout, you need to sign-in again!",
			'th' => "หมดเวลา, คุณต้อง sign-in เข้าระบบอีกครั้ง!",
		]);
	}

	public static function withNeedSignIn(string $message = '')
	{
		return new self($message, [
			'en' => "You require sign-in!",
			'th' => "คุณต้อง sign-in เข้าระบบ!",
		]);
	}

	public static function withPasswordIsIncorrect(string $message = '')
	{
		return new self($message, [
			'en' => "Password is incorrect!",
			'th' => "รหัสผ่านไม่ถูกต้อง!",
		]);
	}

	public static function withEmailOrPasswordInvalid(string $message = '')
	{
		return new self($message, [
			'en' => "Your email or password is incorrect!",
			'th' => "คุณใส่อีเมลหรือรหัสผ่านไม่ถูกต้อง!",
		]);
	}

	public static function withMemberIsResigned(string $message = '')
	{
		return new self($message, [
			'en' => "Your membership is resigned!",
			'th' => "คุณลาออกจากระบบไปแล้ว!",
		]);
	}

	public static function withMemberIsDisabledByAdmin(string $message = '')
	{
		return new self($message, [
			'en' => "Your membership is disabled!",
			'th' => "คุณถูกระงับการใช้งาน!",
		]);
	}

	public static function withMemberRequired(string $message = '')
	{
		return new self($message, [
			'en' => "Member rights is required!",
			'th' => "ต้องการสิทธิ Member!",
		]);
	}

	public static function withAdminRequired(string $message = '')
	{
		return new self($message, [
			'en' => "Admin rights is required!",
			'th' => "ต้องการสิทธิ Admin!",
		]);
	}

	public static function withExternalError(string $message = '')
	{
		return new self($message, [
			'en' => "External sign-in error!",
			'th' => "การเข้าสู่ระบบข้างนอก ล้มเหลว!",
		]);
	}
}
