<?php

namespace App\Exceptions;

class BadRequestException extends CustomHttpException
{
	// 400 Bad Request
	public function __construct(string $message, array $locales)
	{
		parent::__construct(400, $message, $locales);
	}

	public static function withJsonSyntax(string $message = '')
	{
		return new self($message, [
			'en' => "JSON is malform!",
			'th' => "JSON ไม่ถูกต้อง!",
		]);
	}

	public static function withValidationFailed(string $message = '')
	{
		return new self($message, [
			'en' => "Validation is failed!",
			'th' => "การตรวจสอบข้อมูลไม่ถูกต้อง!",
		]);
	}

	public static function withUploadIsNotFound(string $message = '')
	{
		return new self($message, [
			'en' => "Uploaded file is not found!",
			'th' => "ไม่มีไฟล์ที่ Upload ขึ้นมา!",
		]);
	}

	public static function withUploadIsNotType(string $message = '')
	{
		return new self($message, [
			'en' => "Uploaded file is not a specify type!",
			'th' => "ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์ชนิดที่ต้องการ!",
		]);
	}

	public static function withUploadIsNotTypeImage(string $message = '')
	{
		return new self($message, [
			'en' => "Uploaded file is not an image file!",
			'th' => "ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์รูปภาพ!",
		]);
	}

	public static function withUploadIsTooBig(string $message = '', string $limit = null)
	{
		if (!$limit || $limit === '') {
			return new self($message, [
				'en' => "Uploaded file size is too big!",
				'th' => "ไฟล์ที่ Upload ขึ้นมา มีขนาดใหญ่เกินไป!",
			]);
		} else {
			return new self($message, [
				'en' => "Uploaded file size is too big! (Limit $limit)",
				'th' => "ไฟล์ที่ Upload ขึ้นมา มีขนาดใหญ่เกินไป! (ขนาดใหญ่สุด $limit)",
			]);
		}
	}
}
