<?php

namespace App\Exceptions;

class ForbiddenException extends CustomHttpException
{
	// 403 Forbidden
	public function __construct(string $message, array $locales)
	{
		parent::__construct(403, $message, $locales);
	}

	public static function withAlreadyExists(string $message = '')
	{
		return new self($message, [
			'en' => "Data is already exists!",
			'th' => "มีข้อมูลที่ต้องการอยู่แล้ว!",
		]);
	}
}
