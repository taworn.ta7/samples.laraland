<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomHttpException extends HttpException
{
	public $locales = null;

	function __construct(int $status, string $message, array $locales)
	{
		parent::__construct($status, $message);
		$this->locales = $locales;
	}

	function getLocale(string $locale = null)
	{
		if ($locale && array_key_exists($locale, $this->locales))
			return $this->locales[$locale];
		else
			return $this->locales['en'];
	}
}
