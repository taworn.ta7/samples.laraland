<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_resets', function (Blueprint $table) {
            $table->id();
            $table->string('email', 254);
            $table->string('confirm_token', 128)->nullable()->unique();
            $table->timestamp('created')->nullable();
            $table->timestamp('updated')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_resets');
    }
};
