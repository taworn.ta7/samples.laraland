<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->string('email', 254)->unique();
            $table->string('role', 50);
            $table->string('locale', 10);
            $table->string('name', 200);
            $table->dateTime('disabled')->nullable();
            $table->dateTime('resigned')->nullable();
            $table->dateTime('begin')->nullable();
            $table->dateTime('end')->nullable();
            $table->dateTime('expire')->nullable();
            $table->timestamp('created')->nullable();
            $table->timestamp('updated')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};
