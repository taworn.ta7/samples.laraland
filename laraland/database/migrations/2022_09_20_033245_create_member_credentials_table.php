<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_credentials', function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->string('salt', 255);
            $table->string('hash', 1024);
            $table->string('token', 256)->nullable()->unique();
            $table->timestamp('created')->nullable();
            $table->timestamp('updated')->nullable();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_credentials');
    }
};
