<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'common.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'setup.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'members', 'signup.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'members', 'reset.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'members', 'members.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'authen.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'profile', 'icon.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'profile', 'name.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'profile', 'pass.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'settings', 'change.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'settings', 'generic.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'api', 'admin', 'members.php'));
