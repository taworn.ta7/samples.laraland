<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/



require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'common.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'members', 'signup.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'members', 'reset.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'members', 'members.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'profile', 'icon.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'profile', 'name.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'profile', 'pass.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'authenx', 'google.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'web', 'authenx', 'line.php'));
