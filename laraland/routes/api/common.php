<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;



// ----------------------------------------------------------------------
// Common Services
// ----------------------------------------------------------------------

/**
 * Returns server name and version.
 */
Route::get('/about', function (Request $request) {
	return [
		'app'     => config('app.name'),
		'version' => config('env.version'),
	];
});

/**
 * Returns current configuration.
 */
Route::get('/config', function (Request $request) {
	return array_merge(config('app'), config('env'));
});
