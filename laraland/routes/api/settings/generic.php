<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'settings', 'generic.php'));



// ----------------------------------------------------------------------
// Settings Services / Generic Settings
// ----------------------------------------------------------------------

/**
 * Sets key-value generic settings.
 */
Route::put('/settings/{key}/{value}', function (Request $request, string $key, string $value) {
	$member = $request->attributes->get('member');
	return \App\Classes\settingsPut($member, $key, $value);
})->middleware(\App\Http\Middleware\Authen::class);

/**
 * Gets key-value generic settings.  Returns value string, or null if key not exists.
 */
Route::get('/settings/{key}', function (Request $request, string $key) {
	$member = $request->attributes->get('member');
	return \App\Classes\settingsGet($member, $key);
})->middleware(\App\Http\Middleware\Authen::class);

/**
 * Gets all generic settings.  Returns all key-value pairs.
 */
Route::get('/settings', function (Request $request) {
	$member = $request->attributes->get('member');
	return \App\Classes\settingsGetAll($member);
})->middleware(\App\Http\Middleware\Authen::class);

/**
 * Deletes all generic settings.  This function is use for reset.
 */
Route::put('/settings', function (Request $request) {
	$member = $request->attributes->get('member');
	return \App\Classes\settingsReset($member);
})->middleware(\App\Http\Middleware\Authen::class);
