<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Settings Services / Change Settings
// ----------------------------------------------------------------------

/**
 * Changes current settings.  Currently, it has just one setting: locale.
 */
Route::put('/settings/change', function (Request $request) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');

	// validates request
	try {
		$input = $request->validate([
			'member.locale' => 'required|in:en,th',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	// change settings
	$member->locale = $request->input('member.locale');
	$member->save();

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email settings changed.",
	]);

	return [
		'member' => reduceMemberData($member),
	];
})->middleware(\App\Http\Middleware\Authen::class);
