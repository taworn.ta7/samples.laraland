<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Setup Services
// ----------------------------------------------------------------------

/**
 * Setup and creates admin member.  Can run only once.
 */
Route::post('/setup', function (Request $request) {
	$request_id = $request->attributes->get('request_id');

	// checks if we already have 'admin' member?
	$email = config('mail.from.address');
	$member = \App\Models\Member::where('email', $email)
		->where('role', \App\Models\MemberRole::Admin->value)
		->first();
	if ($member)
		throw \App\Exceptions\ForbiddenException::withAlreadyExists("Already setup.");

	// creates 'admin'
	$password = Hash::make('admin');
	DB::beginTransaction();
	try {
		$member = \App\Models\Member::create([
			'email' => $email,
			'role' => \App\Models\MemberRole::Admin->value,
			'locale' => 'en',
			'name' => 'Administrator',
			'disabled' => null,
			'resigned' => null,
			'begin' => null,
			'end' => null,
			'expire' => null,
		]);
		$credential = \App\Models\MemberCredential::create([
			'id' => $member->id,
			'salt' => '',
			'hash' => $password,
			'token' => null,
		]);
		DB::commit();
	} catch (\Exception $ex) {
		DB::rollBack();
		throw \App\Exceptions\InternalServerException::withDbTransaction($ex->getMessage());
	}

	Log::info("$request_id; admin created: {$member->toJson(JSON_PRETTY_PRINT)}");
	return [
		'ok' => "Setup completed :)",
	];
});
