<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'members', 'signup.php'));



// ----------------------------------------------------------------------
// Members Services / Sign Up
// ----------------------------------------------------------------------

/**
 * Receives sign-up form and saves it to database.  Returns URL to let user confirm it.
 */
Route::post('/members/signup', function (Request $request) {
	$request_id = $request->attributes->get('request_id');

	// validates request
	try {
		$input = $request->validate([
			'member.email' => 'required|email',
			'member.password' => 'required|min:4|max:20',
			'member.confirmPassword' => 'required|min:4|max:20|same:member.password',
			'member.locale' => 'required|in:en,th',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	return \App\Classes\membersSignUp($request, $request->input('member.email'), $request->input('member.password'), $request->input('member.locale'));
});

/**
 * Confirms the sign-up form, then, copies sign-up data into member table.
 */
Route::get('/members/signup/confirm', function (Request $request) {
	return \App\Classes\membersSignUpConfirm($request);
});
