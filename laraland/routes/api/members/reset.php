<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'members', 'reset.php'));



// ----------------------------------------------------------------------
// Members Services / Sign Up
// ----------------------------------------------------------------------

/**
 * Receives password reset form and sends email to member.
 */
Route::post('/members/request-reset', function (Request $request) {
	$request_id = $request->attributes->get('request_id');

	// validates request
	try {
		$input = $request->validate([
			'member.email' => 'required|email',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	return \App\Classes\membersRequestReset($request, $request->input('member.email'));
});

/**
 * Confirms the password reset.  This will generate new password and send new password to email.
 */
Route::get('/members/reset-password', function (Request $request) {
	return \App\Classes\membersReset($request);
});
