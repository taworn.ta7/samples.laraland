<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'members', 'members.php'));



// ----------------------------------------------------------------------
// Members Services
// ----------------------------------------------------------------------

/**
 * Lists members with conditions.  All parameters are optional.
 */
Route::get('/members', function (Request $request) {
	return \App\Classes\membersList($request);
});

/**
 * Gets member's information.
 */
Route::get('/members/{email}', function (Request $request, string $email) {
	return \App\Classes\membersGet($request, $email);
});

/**
 * Gets member's icons.
 */
Route::get('/members/{email}/icon', function (Request $request, string $email) {
	return \App\Classes\membersGetIcon($request, $email);
});
