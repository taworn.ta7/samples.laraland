<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'profile', 'pass.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Password
// ----------------------------------------------------------------------

/**
 * Changes the current password.  After the password is changed, you will be sign-out and need to sign-in again.
 */
Route::put('/profile/password', function (Request $request) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');

	// validates request
	try {
		$input = $request->validate([
			'member.currentPassword' => 'required|min:4|max:20',
			'member.newPassword' => 'required|min:4|max:20',
			'member.confirmPassword' => 'required|min:4|max:20|same:member.newPassword',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	return \App\Classes\profileChangePass($request, $member, $request->input('member.currentPassword'), $request->input('member.newPassword'));
})->middleware(\App\Http\Middleware\Authen::class);
