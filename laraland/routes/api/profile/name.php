<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'profile', 'name.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Name
// ----------------------------------------------------------------------

/**
 * Changes the current name.
 */
Route::put('/profile/name', function (Request $request) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');

	// validates request
	try {
		$input = $request->validate([
			'member.name' => 'required',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	return \App\Classes\profileChangeName($request, $member, $request->input('member.name'));
})->middleware(\App\Http\Middleware\Authen::class);
