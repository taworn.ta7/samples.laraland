<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'profile', 'icon.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Icon
// ----------------------------------------------------------------------

/**
 * Uploads picture to use as member profile icon.
 */
Route::post('/profile/icon', function (Request $request) {
	return \App\Classes\profileIconUpload($request);
})->middleware(\App\Http\Middleware\Authen::class);

/**
 * Views the uploaded profile icon.
 */
Route::get('/profile/icon', function (Request $request) {
	return \App\Classes\profileIconView($request);
})->middleware(\App\Http\Middleware\Authen::class);

/**
 * Deletes the uploaded profile icon and reverts profile icon to defaults.
 */
Route::delete('/profile/icon', function (Request $request) {
	return \App\Classes\profileIconDelete($request);
})->middleware(\App\Http\Middleware\Authen::class);
