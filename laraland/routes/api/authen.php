<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'authen.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'app', 'Classes', 'authen.php'));



// ----------------------------------------------------------------------
// Authentication Services
// ----------------------------------------------------------------------

/**
 * Authorizes the email and password.  Returns member data and sign-in token and will be use all the session.
 */
Route::put('/authen/signin', function (Request $request) {
	// validates request
	try {
		$input = $request->validate([
			'signin.email' => 'required',
			'signin.password' => 'required|min:4|max:20',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	return \App\Classes\authenSignIn($request, $request->input('signin.email'), $request->input('signin.password'));
});

/**
 * Signs off from current session.  The sign-in token will be invalid.
 */
Route::put('/authen/signout', function (Request $request) {
	return \App\Classes\authenSignOut($request);
})->middleware(\App\Http\Middleware\Authen::class);

/**
 * Checks current sign-in state.
 */
Route::get('/authen/check', function (Request $request) {
	$member = $request->attributes->get('member');
	return [
		'member' => reduceMemberData($member),
	];
})->middleware(\App\Http\Middleware\Authen::class);
