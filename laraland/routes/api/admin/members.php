<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'admin', 'authorize.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'admin', 'disabled.php'));



// ----------------------------------------------------------------------
// Administration Services / Members
// ----------------------------------------------------------------------

/**
 * Authorizes the member signing-up.  This service is primary used in testing only.
 */
Route::put('/admin/members/authorize', function (Request $request) {
	return \App\Classes\adminMembersAuthorize($request);
})->middleware(\App\Http\Middleware\AuthenAdmin::class);

// ----------------------------------------------------------------------

/**
 * Disables or enables a member.
 */
Route::put('/admin/members/disable/{email}', function (Request $request, string $email) {
	$request_id = $request->attributes->get('request_id');
	$admin = $request->attributes->get('member');

	// validates request
	try {
		$input = $request->validate([
			'member.disabled' => 'required|boolean',
		]);
		//dumpvar($input, "validated input");
	} catch (\Exception $ex) {
		throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
	}

	$disabled = $request->input('member.disabled') ? true : false;
	return \App\Classes\adminMembersDisable($request, $email, $disabled);
})->middleware(\App\Http\Middleware\AuthenAdmin::class);
