<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'helpers', 'web.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'app', 'Classes', 'authen.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', 'app', 'Classes', 'settings', 'generic.php'));



// ----------------------------------------------------------------------
// Common Services
// ----------------------------------------------------------------------

/**
 * This route will be multi-function landing page:
 * - if you not sign-in: you get BeginPage
 * - if you sign-in: you get DashboardPage.
 */
Route::get('/', function (Request $request) {
	$member = $request->attributes->get('member');
	if (!$member) {
		$url = urldecode($request->query('url', '/'));
		return view('begin', [
			'url' => $url,
		]);
	}

	return view('home', [
		'member' => $member,
		'settings' => $request->attributes->get('settings'),
	]);
})->middleware(\App\Http\Middleware\AuthenUi::class);

/**
 * This route will check email and password.  If anything corrent, it signed-in.
 */
Route::post('/signin', function (Request $request) {
	$request_id = $request->attributes->get('request_id');

	try {
		// validates request
		try {
			$input = $request->validate([
				'email' => 'required',
				'password' => 'required|min:4|max:20',
				'url' => '',
			]);
			//dumpvar($input, "validated input");
		} catch (\Exception $ex) {
			throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
		}

		// sign-in
		$result = \App\Classes\authenSignIn($request, $request->input('email'), $request->input('password'));
	} catch (\Exception $ex) {
		$url = urldecode($request->input('url', '/'));
		return view('begin', [
			'error' => exceptionToClient($request, $ex),
			'url' => $url,
		]);
	}

	$member = $result['member'];
	$token = $result['token'];
	$request->session()->put('token', $token);
	return redirect($request->input('url') ?? '/');
});

/**
 * This route is same /signin but this is GET, it will check as same as route /.
 */
Route::get('/signin', function (Request $request) {
	$member = $request->attributes->get('member');
	if ($member) {
		$url = urldecode($request->query('url', '/'));
		return redirect(urlencode($url));
	}

	$url = urldecode($request->query('url'));
	return redirect('/?url=' . urlencode($url));
})->middleware(\App\Http\Middleware\AuthenUi::class);

/**
 * This route will sign-out and go to route /.
 */
Route::get('/signout', function (Request $request) {
	\App\Classes\authenSignOut($request);
	$request->session()->pull('token');
	return redirect('/');
});
