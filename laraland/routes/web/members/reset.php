<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'members', 'reset.php'));



// ----------------------------------------------------------------------
// Members Services / Sign Up
// ----------------------------------------------------------------------

/**
 * Receives password reset form and sends email to member.
 */
Route::get('/forgot', function (Request $request) {
	return view('signup', []);
});

/**
 * Confirms the password reset.  This will generate new password and send new password to email.
 */
Route::post('/forgot', function (Request $request) {
	$request_id = $request->attributes->get('request_id');

	// validates request
	try {
		try {
			$input = $request->validate([
				'email' => 'required|email',
			]);
			//dumpvar($input, "validated input");
		} catch (\Exception $ex) {
			throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
		}

		\App\Classes\membersRequestReset($request, $request->input('email'));
	} catch (\Exception $ex) {
		return view('signup', [
			'error' => exceptionToClient($request, $ex),
		]);
	}

	//return redirect('/');
	return view('warn', [
		'message_en' => "Please check email for your instruction!",
		'message_th' => "โปรดเช็คอีเมลสำหรับคำแนะนำต่อไป!",
	]);
});
