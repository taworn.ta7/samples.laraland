<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'members', 'signup.php'));



// ----------------------------------------------------------------------
// Members Services / Sign Up
// ----------------------------------------------------------------------

/**
 * Receives sign-up form and saves it to database.  Returns URL to let user confirm it.
 */
Route::get('/signup', function (Request $request) {
	return view('signup', []);
});

Route::post('/signup', function (Request $request) {
	$request_id = $request->attributes->get('request_id');

	// validates request
	try {
		try {
			$input = $request->validate([
				'email' => 'required|email',
				'password' => 'required|min:4|max:20',
				'confirmPassword' => 'required|min:4|max:20|same:password',
				'locale' => 'required|in:en,th',
			]);
			//dumpvar($input, "validated input");
		} catch (\Exception $ex) {
			throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
		}

		\App\Classes\membersSignUp($request, $request->input('email'), $request->input('password'), $request->input('locale'));
	} catch (\Exception $ex) {
		return view('signup', [
			'error' => exceptionToClient($request, $ex),
		]);
	}

	//return redirect('/');
	return view('warn', [
		'message_en' => "Please confirm the sign-up by email!",
		'message_th' => "โปรดยืนยันการสมัครสมาชิกทางอีเมล!",
	]);
});
