<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'members', 'members.php'));



// ----------------------------------------------------------------------
// Members Services
// ----------------------------------------------------------------------

/**
 * Lists members with conditions.  All parameters are optional.
 */
Route::get('/members', function (Request $request) {
	$member = $request->attributes->get('member');
	if (!$member) {
		return redirect('/signin?url=' . urlencode('/members'));
	}

	$data = \App\Classes\membersList($request);

	return view('data', [
		'member' => $member,
		'settings' => $request->attributes->get('settings'),
		'data' => $data,
	]);
})->middleware(\App\Http\Middleware\AuthenUi::class);

/**
 * Gets member's information.
 */
Route::get('/members/{email}', function (Request $request, string $email) {
	$member = $request->attributes->get('member');
	if (!$member) {
		return redirect('/signin?url=' . urlencode("/members/$email"));
	}

	try {
		$data = \App\Classes\membersGet($request, $email);
	} catch (\Exception $ex) {
		return view('home', [
			'error' => exceptionToClient($request, $ex),
			'member' => $member,
			'settings' => $request->attributes->get('settings'),
		]);
	}

	return view('data', [
		'member' => $member,
		'settings' => $request->attributes->get('settings'),
		'data' => $data,
	]);
})->middleware(\App\Http\Middleware\AuthenUi::class);

Route::post('/members/{email}', function (Request $request, string $email) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');
	if (!$member) {
		$url = '/profile/change-name';
		return redirect('/signin?url=' . urlencode($url));
	}

	if ($member->role !== \App\Models\MemberRole::Admin->value) {
		return view('home', [
			'error' => exceptionToClient($request, \App\Exceptions\UnauthorizedException::withAdminRequired()),
			'member' => $member,
			'settings' => $request->attributes->get('settings'),
		]);
	}

	try {
		// validates request
		try {
			$input = $request->validate([
				'disable' => '',
			]);
			//dumpvar($input, "validated input");
		} catch (\Exception $ex) {
			throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
		}

		$disable = $request->input('disable') ? true : false;
		\App\Classes\adminMembersDisable($request, $email, $disable);
	} catch (\Exception $ex) {
		return view('home', [
			'error' => exceptionToClient($request, $ex),
			'member' => $member,
			'settings' => $request->attributes->get('settings'),
		]);
	}

	return redirect("/members/$email");
})->middleware(\App\Http\Middleware\AuthenUi::class);
