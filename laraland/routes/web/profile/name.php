<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'web.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'profile', 'name.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Name
// ----------------------------------------------------------------------

/**
 * Changes the current name.
 */
Route::get('/profile/change-name', function (Request $request) {
	return genericResponse($request, '/profile/change-name');
})->middleware(\App\Http\Middleware\AuthenUi::class);

Route::post('/profile/change-name', function (Request $request) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');
	if (!$member) {
		$url = '/profile/change-name';
		return redirect('/signin?url=' . urlencode($url));
	}

	try {
		// validates request
		try {
			$input = $request->validate([
				'name' => 'required',
			]);
			//dumpvar($input, "validated input");
		} catch (\Exception $ex) {
			throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
		}

		\App\Classes\profileChangeName($request, $member, $request->input('name'));
	} catch (\Exception $ex) {
		return view('home', [
			'error' => exceptionToClient($request, $ex),
			'member' => $member,
			'settings' => $request->attributes->get('settings'),
		]);
	}

	return redirect('/profile/change-name');
})->middleware(\App\Http\Middleware\AuthenUi::class);
