<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'web.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'profile', 'pass.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Password
// ----------------------------------------------------------------------

/**
 * Changes the current password.  After the password is changed, you will be sign-out and need to sign-in again.
 */
Route::get('/profile/change-pass', function (Request $request) {
	return genericResponse($request, '/profile/change-pass');
})->middleware(\App\Http\Middleware\AuthenUi::class);

Route::post('/profile/change-pass', function (Request $request) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');
	if (!$member) {
		$url = '/profile/change-pass';
		return redirect('/signin?url=' . urlencode($url));
	}

	try {
		// validates request
		try {
			$input = $request->validate([
				'currentPassword' => 'required|min:4|max:20',
				'newPassword' => 'required|min:4|max:20',
				'confirmPassword' => 'required|min:4|max:20|same:newPassword',
			]);
			//dumpvar($input, "validated input");
		} catch (\Exception $ex) {
			throw \App\Exceptions\BadRequestException::withValidationFailed($ex->getMessage());
		}

		\App\Classes\profileChangePass($request, $member, $request->input('currentPassword'), $request->input('newPassword'));
	} catch (\Exception $ex) {
		return view('home', [
			'error' => exceptionToClient($request, $ex),
			'member' => $member,
			'settings' => $request->attributes->get('settings'),
		]);
	}

	//return redirect('/profile/change-pass');
	return view('warn', [
		'message_en' => "You have to sign-out, then sign-in again with new password!",
		'message_th' => "คุณต้องออกจากระบบ แล้วเข้าระบบใหม่ ด้วยรหัสผ่านใหม่!",
	]);
})->middleware(\App\Http\Middleware\AuthenUi::class);
