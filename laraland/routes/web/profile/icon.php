<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Log;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'web.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'app', 'Classes', 'profile', 'icon.php'));



// ----------------------------------------------------------------------
// Profile Services / Profile Icon
// ----------------------------------------------------------------------

/**
 * Uploads picture to use as member profile icon.
 */
Route::get('/profile/change-icon', function (Request $request) {
	return genericResponse($request, '/profile/change-icon');
})->middleware(\App\Http\Middleware\AuthenUi::class);

Route::post('/profile/change-icon', function (Request $request) {
	$request_id = $request->attributes->get('request_id');
	$member = $request->attributes->get('member');
	if (!$member) {
		$url = '/profile/change-icon';
		return redirect('/signin?url=' . urlencode($url));
	}

	try {
		$hasFile = $request->hasFile('image');
		if ($hasFile)
			\App\Classes\profileIconUpload($request);
		else
			\App\Classes\profileIconDelete($request);
	} catch (\Exception $ex) {
		return view('home', [
			'error' => exceptionToClient($request, $ex),
			'member' => $member,
			'settings' => $request->attributes->get('settings'),
		]);
	}

	return redirect('/profile/change-icon');
})->middleware(\App\Http\Middleware\AuthenUi::class);
