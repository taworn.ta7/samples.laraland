<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
//use Illuminate\Support\Facades\Storage;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'common.php'));



// ----------------------------------------------------------------------
// Authentication External Services
// ----------------------------------------------------------------------

/**
 * Sign-in using LINE.
 */
Route::get('/authenx/line', function (Request $request) {
	$code = $request->query('code');
	$state = $request->query('state');

	$request_id = $request->attributes->get('request_id');

	// retrieves for token
	$url = "https://api.line.me/oauth2/v2.1/token";
	$response =  Http::asForm()->withHeaders([
		'Content-Type' => 'application/x-www-form-urlencoded',
	])->post($url, [
		'code' => $code,
		'client_id' => config('env.line_client_id'),
		'client_secret' => config('env.line_client_secret'),
		'redirect_uri' => config('env.line_redirect_url'),
		'grant_type' => 'authorization_code',
	]);
	dumpvar([
		'code' => $code,
		'client_id' => config('env.line_client_id'),
		'client_secret' => config('env.line_client_secret'),
		'redirect_uri' => config('env.line_redirect_url'),
		'grant_type' => 'authorization_code',
	]);
	$status = $response->status();
	if ($status !== 200) {
		Log::debug("$request_id; $status: $url");
		throw \App\Exceptions\UnauthorizedException::withExternalError();
	}
	$token = $response->json();
	dumpvar($token, "$request_id; token");
	if (!$token['access_token'] || !$token['id_token']) {
		throw \App\Exceptions\UnauthorizedException::withExternalError();
	}

	// retrieves for member information
	$cid = config('env.line_client_id');
	$url = "https://api.line.me/oauth2/v2.1/verify?id_token={$token['id_token']}&client_id=$cid";
	$response =  Http::post($url);
	$status = $response->status();
	if ($status !== 200) {
		Log::debug("$request_id; $status: $url");
		throw \App\Exceptions\UnauthorizedException::withExternalError();
	}
	$info = $response->json();
	dumpvar($info, "$request_id; member");
	if (!$info['email'] || !$info['name']) {
		throw \App\Exceptions\UnauthorizedException::withExternalError();
	}

	// creates or loads member
	$result = memberForExternalSignIn($request, $info['email'], 'en', $info['name']);
	$member = $result['member'];

	// adds log
	\App\Models\Logging::create([
		'member_id' => $member->id,
		'action' => 'update',
		'table' => 'member',
		'description' => "Member $member->id/$member->email is sign-in from LINE.",
	]);

	/*
	 * NOTE: 
	 * not work! 
	// if just created and have profile
	//if ($result['created'] && $info['picture']) {
	if ($info['picture']) {
		// loads profile icon from host
		$response = Http::get($info['picture']);
		$status = $response->status();
		if ($status === 200) {
			$dir = "public/$member->id/profile";
			//$part = "storage/$member->id/profile";
			$name = 'icon';
			Storage::disk('local')->putFileAs($dir, $response->object(), $name);
		}
	}
	*/

	$request->session()->put('token', $result['token']);
	return redirect('/');
});
