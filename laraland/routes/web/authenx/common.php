<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', 'helpers', 'helpers.php'));



// ----------------------------------------------------------------------
// Authentication External Services
// ----------------------------------------------------------------------

/**
 * Prepares member data for external sign-in.
 */
function memberForExternalSignIn(Request $request, string $email, string $locale, string $name)
{
	$request_id = $request->attributes->get('request_id');

	// checks if email is exists
	$member = \App\Models\Member::where('email', $email)->first();

	// not found, creates it
	if (!$member) {
		// checks locale
		if ($locale !== 'en' && $locale !== 'th')
			$locale = 'en';

		// creates member
		$generate = Str::random(8);
		$password = Hash::make($generate);
		$member = \App\Models\Member::create([
			'email' => $email,
			'role' => \App\Models\MemberRole::Member->value,
			'locale' => $locale,
			'name' => $name,
			'disabled' => null,
			'resigned' => null,
			'begin' => null,
			'end' => null,
			'expire' => null,
		]);
		$credential = \App\Models\MemberCredential::create([
			'id' => $member->id,
			'salt' => '',
			'hash' => $password,
			'token' => null,
		]);

		// updates saves
		DB::transaction(function () use ($member, $credential) {
			$member->save();
			$credential->save();
		});

		// reloads member
		$member = \App\Models\Member::where('email', $email)->first();
		$member->credential();
		$token = newOrUpdateToken($member);
		DB::transaction(function () use ($member) {
			$member->save();
			$member->credential->save();
		});

		// adds log
		\App\Models\Logging::create([
			'member_id' => $member->id,
			'action' => 'create',
			'table' => 'member',
			'description' => "Member $member->id/$member->email is created via external sign-in.",
		]);

		dumpvar($member, "$request_id; member created");
		return [
			'member' => reduceMemberData($member),
			'token' => $token,
			'created' => true,
		];
	} else {
		// checks if this member resigned or disabled
		if ($member->resigned)
			throw \App\Exceptions\UnauthorizedException::withMemberIsResigned();
		if ($member->disabled)
			throw \App\Exceptions\UnauthorizedException::withMemberIsDisabledByAdmin();

		// updates token and saves
		$member->credential();
		$token = newOrUpdateToken($member);
		DB::transaction(function () use ($member) {
			$member->save();
			$member->credential->save();
		});

		dumpvar($member, "$request_id; member loaded");
		return [
			'member' => reduceMemberData($member),
			'token' => $token,
			'created' => false,
		];
	}
}
