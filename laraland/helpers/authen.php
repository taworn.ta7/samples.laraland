<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Firebase\JWT\JWT;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'helpers.php'));



// ----------------------------------------------------------------------
// Authentication Helpers
// ----------------------------------------------------------------------

/**
 * Creates or updates token in member.  The member structure must include credential.
 */
function newOrUpdateToken(\App\Models\Member $member): string
{
	$token = '';
	$now = millitime();
	$expire = $now + config('env.authen_timeout');

	if ($member->credential->token && $member->expire && $member->expire->getTimestampMs() >= $now) {
		// updates expiry sign-in
		$member->expire = \Carbon\Carbon::createFromTimestampMs($expire);
		$token = $member->credential->token;
	} else {
		// signs token
		$payload = [
			'id' => $member->id,
			'sub' => 'Laraland',
		];
		$secret = Str::random(32);
		$token = JWT::encode($payload, $secret, 'HS256');

		// updates member
		$member->begin = \Carbon\Carbon::createFromTimestampMs($now);
		$member->end = null;
		$member->expire = \Carbon\Carbon::createFromTimestampMs($expire);
		$member->credential->token = $token;
	}

	return $token;
}

// ----------------------------------------------------------------------

function tokenFromHeaders(Request $req): string|null
{
	$auth = $req->header('authorization');
	if ($auth) {
		$a = preg_split('/ /', $auth);
		if (count($a) >= 2) {
			if (strtolower($a[0]) === 'bearer') {
				return $a[1];
			}
		}
	}
	return null;
}

function memberFromHeaders(Request $req): \App\Models\Member|null
{
	$token = tokenFromHeaders($req);
	if (!$token)
		throw \App\Exceptions\UnauthorizedException::withNeedSignIn();

	// searches in members by token
	$credential = \App\Models\MemberCredential::where('token', $token)->first();
	if (!$credential)
		throw \App\Exceptions\UnauthorizedException::withNeedSignIn();
	$member = $credential->member;
	$member->credential;

	// checks if session is expired
	$now = millitime();
	if ($member->expire->getTimestampMs() < $now)
		throw \App\Exceptions\UnauthorizedException::withTimeout();

	// updates expiry sign-in
	$expire = $now + config('env.authen_timeout');
	$member->expire = \Carbon\Carbon::createFromTimestampMs($expire);
	$member->save();
	return $member;
}

function memberFromToken(?string $token): \App\Models\Member|null
{
	// checks token
	if (!$token && trim($token) === "")
		throw \App\Exceptions\UnauthorizedException::withNeedSignIn();

	// searches in members by token
	$credential = \App\Models\MemberCredential::where('token', $token)->first();
	if (!$credential)
		throw \App\Exceptions\UnauthorizedException::withNeedSignIn();
	$member = $credential->member;
	$member->credential;

	// checks if session is expired
	$now = millitime();
	if ($member->expire->getTimestampMs() < $now)
		throw \App\Exceptions\UnauthorizedException::withTimeout();

	// updates expiry sign-in
	$expire = $now + config('env.authen_timeout');
	$member->expire = \Carbon\Carbon::createFromTimestampMs($expire);
	$member->save();
	return $member;
}

// ----------------------------------------------------------------------

/**
 * Creates new member which removes credential data.
 */
function reduceMemberData(\App\Models\Member $member): \App\Models\Member
{
	$reduce = new \App\Models\Member();
	$reduce->id = $member->id;
	$reduce->email = $member->email;
	$reduce->role = $member->role;
	$reduce->locale = $member->locale;
	$reduce->name = $member->name;
	$reduce->disabled = $member->disabled;
	$reduce->resigned = $member->resigned;
	$reduce->begin = $member->begin;
	$reduce->end = $member->end;
	$reduce->expire = $member->expire;
	$reduce->created = $member->created;
	$reduce->updated = $member->updated;
	return $reduce;
}
