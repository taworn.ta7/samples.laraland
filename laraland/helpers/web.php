<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

require_once join(DIRECTORY_SEPARATOR, array(__DIR__, 'helpers.php'));



// ----------------------------------------------------------------------
// Web Helpers
// ----------------------------------------------------------------------

function exceptionToClient(Request $request, \Exception $ex): array
{
	$request_id = $request->attributes->get('request_id');
	$json = [
		'statusCode' => null,
		'message'    => $ex->getMessage(),
		'locales'    => null,
		'path'       => $request->path(),
		'requestId'  => $request_id,
		'timestamp'  => date('c'),
	];
	if ($ex instanceof HttpException) {
		$json['statusCode'] = $ex->getStatusCode();
	}
	if ($ex instanceof \App\Exceptions\CustomHttpException) {
		$json['locales'] = $ex->locales;
	}
	return $json;
}

function genericResponse(Request $request, string $url)
{
	$member = $request->attributes->get('member');
	if (!$member) {
		return redirect('/signin?url=' . urlencode($url));
	}

	return view('home', [
		'member' => $member,
		'settings' => $request->attributes->get('settings'),
	]);
}
