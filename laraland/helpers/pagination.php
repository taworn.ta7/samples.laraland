<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;



// ----------------------------------------------------------------------
// Pagination Helpers
// ----------------------------------------------------------------------

/**
 * Gets pagination from page, rows per page and count.
 */
function getPagination(int $page, int $rowsPerPage, int $count): array
{
	$offset = $page * $rowsPerPage;
	$pageCount = (int)ceil($count / $rowsPerPage);
	$pageSize = $page < $pageCount - 1
		? $rowsPerPage
		: ($page >= $pageCount ? 0 : $count - $offset);
	return [
		'page'        => $page,
		'offset'      => $offset,
		'rowsPerPage' => $rowsPerPage,
		'count'       => $count,
		'pageIndex'   => $page,
		'pageCount'   => $pageCount,
		'pageStart'   => $offset,
		'pageStop'    => $offset + $pageSize,
		'pageSize'    => $pageSize,
	];
}

/**
 * Gets ordering data.
 */
function getOrderingData(array $sortDict, string $order): array
{
	$result = [];
	$parts = preg_split("/[ \t.,:;]+/", $order);
	$length = count($parts);
	for ($i = 0; $i < $length; $i++) {
		$item = trim($parts[$i]);
		$pattern = "/([a-zA-Z0-9_]+)([\+\-]?)/";
		if (preg_match($pattern, $item, $matches)) {
			$field = $matches[1];
			$reverse = $matches[2];
			if (array_key_exists($field, $sortDict)) {
				$key = $sortDict[$field];
				$result[$key] = $reverse === '-' ? 'DESC' : 'ASC';
			}
		}
	}
	return $result;
}

/**
 * Gets current request and extract page, order, search and trash.
 * 
 * @example
 * const { page, order, search, trash } = queryGenericData(req, {
 *   // sort dictionary
 *   email: 'email',
 *   name: 'name',
 *   created: 'created',
 *   updated: 'updated',
 * });
 */
function queryPagingData(Request $req, array $sortDict): array
{
	$page = intval($req->query('page', ''));
	if (!$page || $page < 0)
		$page = 0;
	$order = $sortDict ? getOrderingData($sortDict, $req->query('order', '')) : [];
	$search = trim($req->query('search', ''));
	$trash = boolval($req->query('trash', '') ?? 0 !== 0);
	return [
		'page'   => $page,
		'order'  => $order,
		'search' => $search,
		'trash'  => $trash,
	];
}
