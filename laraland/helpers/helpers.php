<?php

use Illuminate\Support\Facades\Log;



// ----------------------------------------------------------------------
// Helpers
// ----------------------------------------------------------------------

function dumpvar($v, $name = null)
{
	$back = debug_backtrace();
	$file = basename($back[0]['file']);
	$line = $back[0]['line'];

	if (!$name)
		$prefix = "";
	else
		$prefix = "$name: ";

	$type = get_debug_type($v);
	$out = json_encode($v, JSON_PRETTY_PRINT);
	Log::debug("$file($line): $prefix$out $type");
}

function millitime(): int
{
	if (PHP_INT_SIZE >= 8) {
		$mt = explode(' ', microtime());
		return ((int)$mt[1]) * 1000 + ((int)round($mt[0] * 1000));
	} else {
		return floor(microtime(true) * 1000);
	}
}
