/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap'

import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import { createPinia } from 'pinia'
import { messages as en } from '../locales/en'
import { messages as th } from '../locales/th'
import router from './router'

/**
 * Loads vue-i18n.
 */

const i18n = createI18n({
	locale: 'en',
	fallbackLocale: 'en',
	messages: {
		en,
		th,
	},
})

/**
 * Next, we will create a fresh Vue application instance. You may then begin
 * registering components with the application instance so they are ready
 * to use in your application's views. An example is included for you.
 */

import App from './App.vue'
import AppBox from './layouts/AppBox.vue'

const app = createApp(App)
	.use(i18n)
	.use(router)
	.use(createPinia())
	.component('AppBox', AppBox)

//import ExampleComponent from './components/ExampleComponent.vue'
//app.component('example-component', ExampleComponent)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// Object.entries(import.meta.glob('./**/*.vue', { eager: true })).forEach(([path, definition]) => {
//     app.component(path.split('/').pop().replace(/\.\w+$/, ''), definition.default)
// })

/**
 * Finally, we will attach the application instance to a HTML element with
 * an "id" attribute of "app". This element is included with the "auth"
 * scaffolding. Otherwise, you will need to add an element yourself.
 */

app.mount('#app')
