import { defineStore } from 'pinia'
//import Authen from '@/helpers/Authen'
//import AppShare from '@/helpers/AppShare'
import { useLocaleStore } from '@/stores/LocaleStore'
import { useThemeStore } from '@/stores/ThemeStore'
import { useDialogStore } from '@/stores/DialogStore'

/**
 * Sign-in state type.
 */
export class SignInStateType {
	static NowLoading = 0
	static WaitSignIn = -1
	static AlreadySignIn = 1
}

/**
 * Application shared service singleton class.
 */
export const useAppStore = defineStore({

	id: 'AppStore',

	state: () => ({
		//token: '',
		member: null,
		icon: null,
		settings: {},
		state: SignInStateType.AlreadySignIn,
	}),

	actions: {
		/**
		 * Loads previous session.
		 */
		async setup() {
			let element = document.querySelector('meta[name="member"]')
			if (element) {
				const member = element.getAttribute('content');
				if (member) {
					console.log(`member sign-in: ${JSON.stringify(member, null, 2)}`)
					this.member = JSON.parse(member)
					this.icon = `/api/members/${this.member.email}/icon`
					element = document.querySelector('meta[name="settings"]')
					this.settings = element ? JSON.parse(element.getAttribute('content')) : {}
					this.state = SignInStateType.AlreadySignIn
					return true
				}
			}
			console.log(`no member sign-in`)
			this.member = null
			this.icon = null
			this.settings = {}
			this.state = SignInStateType.WaitSignIn
			return false
		},

		// ----------------------------------------------------------------------

		/**
		 * Saves locale settings.
		 */
		async saveLocale(locale) {
			/*
			const result = await AppShare.client.call(`settings/change`, {
				method: 'PUT',
				headers: AppShare.client.defaultHeaders(this.token),
				data: {
					member: {
						locale,
					},
				},
			})
			if (!result || !result.ok) return
			this.member = result.json.member
			*/
		},

		/**
		 * Saves theme settings.
		 */
		async saveTheme(index) {
			/*
			const value = index.toString()
			const result = await AppShare.client.call(`settings/web-theme/${value}`, {
				method: 'PUT',
				headers: AppShare.client.defaultHeaders(this.token),
			})
			if (!result || !result.ok) return
			this.settings['web-theme'] = value
			*/
		},
	},

})
