import { defineStore } from 'pinia'

export const useDialogStore = defineStore({

	id: 'DialogStore',

	state: () => ({
		box: null,
	}),

	actions: {
		/**
		 * Opens information box.
		 */
		infoBox(message) {
			this.box.infoBox(message)
			console.log(`infoBox: ${message}`)
		},

		/**
		 * Opens warning box.
		 */
		warningBox(message) {
			this.box.warningBox(message)
			console.log(`warningBox: ${message}`)
		},

		/**
		 * Opens error box.
		 */
		errorBox(message) {
			this.box.errorBox(message)
			console.log(`errorBox: ${message}`)
		},

		// ----------------------------------------------------------------------

		/**
		 * Opens wait box and wait for callback is finished.
		 */
		async waitBox(callback) {
			this.box.wait(true)
			console.log(`waitBox: open`)
			const ret = await callback()
			this.box.wait(false)
			console.log(`waitBox: close`)
			return ret
		},

		// ----------------------------------------------------------------------

		/**
		 * Handles REST error, if it's an error.
		 */
		handleRestError(result) {
			if (result && result.ok)
				return false
			return this.box.errorRest(result)
		},

		// ----------------------------------------------------------------------

		/**
		 * Changes current locale.
		 */
		changeLocale(locale) {
			this.box.changeLocale(locale)
		},
	},

})
