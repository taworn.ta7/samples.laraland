import { defineStore } from 'pinia'
import { useDialogStore } from '@/stores/DialogStore'

export const useLocaleStore = defineStore({

	id: 'LocaleStore',

	state: () => ({
		/**
		 * Current locale.
		 */
		current: localStorage.getItem('locale') || 'en',
	}),

	actions: {
		/**
		 * Setup locale.
		 */
		setup() {
			this.change(this.current)
		},

		/**
		 * Changes current locale.
		 */
		change(locale) {
			const dialogStore = useDialogStore()
			dialogStore.changeLocale(locale)
			this.current = locale
			console.log(`current locale: ${this.current}`)
		},
	},

})
