export const Constants = {

	/**
	 * Local base URL.
	 */
	localUrl: 'http://localhost:8000',
	localIpUrl: 'http://127.0.0.1:8000',

	/**
	 * Server base URL.
	 */
	baseUrl: 'http://localhost:8000/api',

	/**
	 * Static server base URL.
	 */
	baseStaticUrl: 'http://localhost:8000',

	/**
	 * Google Client
	 */
	googleClientId: '1028843747070-gs1edoi3su1a29qd6s6vt5kdafdqejqd.apps.googleusercontent.com',
	googleAuthUrl: 'https://accounts.google.com/o/oauth2/v2/auth',
	googleSignIn: () => {
		const scope = [
			'openid',
			'profile',
			'email',
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email',
		]
		return Constants.googleAuthUrl
			+ `?redirect_uri=${Constants.localUrl}/authenx/google`
			+ `&client_id=${Constants.googleClientId}`
			+ `&access_type=offline&response_type=code&prompt=consent`
			+ `&scope=${scope.join(' ')}`
	},

	/**
	 * LINE Client
	 */
	lineClientId: '1657191325',
	lineAuthUrl: 'https://access.line.me/oauth2/v2.1/authorize',
	lineSignIn: () => {
		return Constants.lineAuthUrl
			+ `?redirect_uri=${Constants.localUrl}/authenx/line`
			+ `&client_id=${Constants.lineClientId}`
			+ `&response_type=code&state=${`C0L@F1R3`}`
			+ `&scope=profile%20openid%20email`
	},

}

export default Constants
