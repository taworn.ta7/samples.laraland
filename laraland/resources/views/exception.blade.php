<!DOCTYPE html>
<html>

<head>
	<title>Laraland</title>
	<style>
		.exception {
			font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
			margin: 1rem;
		}

		.exception ul {
			border: 2px solid #f33;
			margin: 0;
			padding: 0;
		}

		.exception ul>li {
			list-style: none;
			margin: 0;
			padding: 0.5rem 1.0rem;
		}

		.exception ul>li:nth-child(odd) {
			background-color: #fff;
		}

		.exception ul>li:nth-child(even) {
			background-color: #ddd;
		}

		.exception ul>li span {
			color: #f66;
		}
	</style>
</head>

<body>
	<div class="exception">
		<h1>Error</h1>
		<ul>
			<li>statusCode: <span>{{ $statusCode }}</span></li>
			<li>message: <span>{{ $message }}</span></li>
			<li>locales: <span>{{ $locales}}</span></li>
			<li>path: <span>{{ $path }}</span></li>
			<li>requestId: <span>{{ $requestId }}</span></li>
			<li>timestamp: <span>{{ $timestamp }}</span></li>
		</ul>
	</div>
</body>

</html>