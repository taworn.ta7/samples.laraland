<!DOCTYPE html>
<html>

<head>
	@if (isset($error) && $error)
	<meta name="error" content="{{ json_encode($error) }}">
	@endif
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="member" content="{{ json_encode($member) }}">
	<meta name="settings" content="{{ json_encode($settings) }}">
	<meta name="data" content="{{ json_encode($data) }}">
	<title>Laraland</title>
	@vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
	<div id="app"></div>
</body>

</html>