<!DOCTYPE html>
<html>

<head>
	<title>Laraland</title>
	<style>
		body {
			margin: 2rem;
		}

		#warn-panel {
			margin-bottom: 2rem;
			padding: 1rem;
			border: 2px solid #cc6;
			border-radius: 1rem;
			background: #ffc;
		}

		#warn-panel div {
			color: #000;
		}
	</style>
</head>

<body>
	<h1>Warning</h1>
	<div id="warn-panel">
		<div>{{ $message_en }}</div>
		<br />
		<div>{{ $message_th }}</div>
	</div>
	<button onclick="window.location.href = '/'">Back</button>
</body>

</html>